#include "GameController.h"


GameController::GameController(string fileName)
{	
	string temp = "../Config/" + fileName + ".json";
	this->levelFileName = temp;
}

void GameController::initGame(string levelFileName)
{

	level.load(level.getMapFromJson(levelFileName), player);
	string name;
	cout << "Enter player name: ";
	cin >> name;
	player.init(name);
	
}
//main loop
void GameController::playGame(bool isNewGame)
{
	if (isNewGame)
	{
		initGame(levelFileName);
	}

	while (isInGame)
	{
		level.print(player);
		playerMove();
		level.MoveEnemies(player);
	}
}

void intro()
{

}
//draw main menu
void GameController::mainMenu()
{
	isInMenu = true;
	while (isInMenu)
	{
		int choice = level.drawMainMenu();
		switch (choice)
		{
		case 1:		// start new game
			isInGame = true;
			isInMenu = false;
			level.clearLevel(player);
			playGame(true);
			break;
		case 2:		// load game
			isInGame = true;
			isInMenu = false;
			loadGame();		
			break;
		case 3:		// quit
			isInMenu = false;
			isInGame = false;			
			break;
		default:
			cin.clear();
			cin.ignore();		
		}
	}
}

//draw in game menu
void GameController::inGameMenu()
{
	isInGameMenu = true;
	while (isInGameMenu)
	{
		int choice = level.drawInGameMenu();
		switch (choice)
		{
		case 1:		// resume
			isInGame = true;
			isInGameMenu = false;
			level.clearConsole();
			playGame(false);
			break;
		case 2:		// save
			isInGameMenu = false;
			saveGame();
			inGameMenu();
			break;		
		case 3:		// load
			if (askForSave())
			{
				saveGame();
			}
			isInGame = true;
			isInGameMenu = false;
			loadGame();
			break;

		case 4:// inventory
			openInventory();
			//level.pauseConsole();
			_getch();

			break;
		case 5:// back to main
			if (askForSave())
			{
				saveGame();
			}			
			isInGameMenu = false;
			mainMenu();
			break;
		case 6://quit
			if (askForSave())
			{
				saveGame();
			}
			isInGameMenu = false;
			isInGame = false;			
			break;
		default:
			cin.clear();
			cin.ignore();

		}
	}
}

//open inventory and equip chosen item
void GameController::openInventory()
{
	int choice = level.drawInventory(player);
	if (choice == -1)
		choice = level.drawInventory(player);

	if (choice != 0)
		player.euqipItem(choice - 1);
}

//save game
void GameController::saveGame()
{
	// list files to chose
	
	string fileName;
	cout << "Enter save file name without extension:";
	cin >> fileName;
	string temp = "../Saved/" + fileName + ".json";
	level.save(player, temp);

}

//load game
void GameController::loadGame()
{
	string fileName;
	cout << "Enter file name you want to load: ";
	cin >> fileName;
	string temp = "../Saved/" + fileName + ".json";
	level.clearLevel(player);

	if(level.load(temp, player))
	{
		playGame(false);
	}
}

// get all data for saving
string GameController::getGameState()
{
	string tempString;
	//tempString.append(player.getAsString());
	//tempString.append(level.getEnemiesStats());
	vector<string> lvlData = level.getLevelData();
	for (int i = 0; i < lvlData.size(); i++)
	{
		tempString.append(lvlData[i]);
		tempString.append("\n");
	}
	return tempString;
}

// get user input
void GameController::playerMove()
{
	char input;
	cout << "\nEnter a move command:[w][a][s][d] " << endl;
	input = _getch();

	if (input == 27) //  open menu on escape button
	{
		inGameMenu();
	}
	else 
	{
		if (!level.movePlayer(input, player))
		{
			playerMove();
		}
	}
}

// ask for save
bool GameController::askForSave()
{
	char input;
	cout << "All unsaved data will be lost! \nDo you want to save the game?  [y][n]";
	cin >> input;

	switch (input)
	{
	case 'Y':
	case 'y':
		return true;
	case 'N':
	case 'n':
		return false;
	}
}
