#include "Enemy.h"
#include<iostream>

Enemy::Enemy(string name, char tile, unsigned short attack, unsigned short defense, unsigned short health,
	unsigned short attackRange, unsigned short expPoints, unsigned short x, unsigned short y)
{
	this->name = name;
	this->symbol = tile;
	this->attack = attack;
	this->defense = defense;
	this->health = health;
	this->attackRange = attackRange;
	this->expPoints = expPoints;
	this->x = x;
	this->y = y;
}

void Enemy::setPosition(unsigned short x, unsigned short y)
{
	this->x = x;
	this->y = y;
}

void Enemy::getPosition(unsigned short &x, unsigned short &y) const
{
	x = this->x;
	y = this->y;
}

unsigned short Enemy::attackMove()
{
	static default_random_engine randomEngine(time(NULL));
	uniform_int_distribution<unsigned short>attackStrength(attack / 2, attack);
	{

	}
	return attackStrength(randomEngine);
}

bool Enemy::takeDamage(int attackValue)
{
	unsigned short tempAttackVal = attackValue;
	attackValue -= defense;
	if (attackValue > 0)
	{
		defense = 0;
		health -= attackValue;
		//check if he died
		if (health <= 0)
		{
			health = 0;
			return true;
		}
	}
	else 
	{
		defense -= tempAttackVal;
		if (defense < 0)
			defense = 0;		
	}
	return false;
}

char Enemy::getMoveCommand(unsigned short playerX, unsigned short playerY)
{	
	//generate random number for movement
	static default_random_engine randomEngine(time(NULL));
	uniform_int_distribution<int>move(0 , 4);

	//if player is in attack range move towards player
	int distance;
	int dx = this->x - playerX;
	int dy = this->y - playerY;
	int absDx = abs(dx);
	int absDy = abs(dy);

	distance = absDx + absDy;

	if (distance < attackRange)
		//moving along X axis
		if (absDx > absDy)
		{
			if (dx > 0)
			{
				return 'a';
			}
			else
			{
				return 'd';
			}

		}
		else // move along Y axis
		{ 
			if (dy > 0)
			{
				return 'w';
			}
			else
			{
				return 's';
			}
		}
	int randomNum = move(randomEngine);
		switch (randomNum)
		{
			case 0:
				return 'a';
			case 1:
				return 's';
			case 3:
				return 'd';
			case 4:
				return 'w';
		}

}

vector<string> Enemy::getAsString() const
{
	vector<string> stats;
	stats.push_back(name);
	stats.push_back(to_string(x));
	stats.push_back(to_string(y));
	stats.push_back(to_string(health));
	stats.push_back(to_string(attack));
	stats.push_back(to_string(defense));
	stats.push_back(to_string(attackRange));
	stats.push_back(to_string(symbol));

	return stats;
}

Enemy::~Enemy()
{
}