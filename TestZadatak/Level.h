#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "Player.h"
#include "Enemy.h"
#include <json/value.h>
#include <json/json.h>

class Level
{
public:
	Level();
	//functions
	
	bool load(string fileName, Player &player);
	void clearLevel(Player &player);
	void print(Player &player);
	void drawIntro(Player &player);
	int drawMainMenu();
	int drawInGameMenu();
	int drawInventory(Player &player);
	bool movePlayer(char input, Player &player);
	void MoveEnemies(Player &player);
	void processPlayerMove(Player &player, unsigned short targetX, unsigned short targetY);
	void clearConsole();
	void save(Player &player, string filename);
	vector<string> getLevelData() const;
	string getMapFromJson(string mapName) const;
	void pauseConsole(string textToShow = "Press enter to continue...");

	//string getEnemiesStats() const;

private:
	vector <string> levelData;
	vector <Enemy> enemies;
	vector <Item> items;
	vector <char> impassibleTiles;

	void processEnemyMove(Player &player, unsigned short index, unsigned short targetX, unsigned short targetY);
	void battleExecution(Player &player, unsigned short targetX, unsigned short targetY, bool whoAttacked);

	void loadInventory(Player &player, string name, unsigned short type, unsigned short value);
	Json::Value processJson(Json::Value arr, unsigned short x, unsigned short y, Player &player);
	void loadItems(string name, unsigned short type, unsigned short value, unsigned short x, unsigned short y, char symbol);
	void CreateEnemies(string name, char tile, unsigned short attack, unsigned short defense, unsigned short health, 
		unsigned short attackRange, unsigned short expPoints, unsigned short x, unsigned short y);
	

	void spawnItem(unsigned short x, unsigned short y);
	void setTile(unsigned short x, unsigned short y, char tile);
	char getTile(unsigned short x, unsigned short y) const;

	char playerSymbol;

	char endGoalSymbol;
	char goalSymbol;
	unsigned short endGoalXPos;
	unsigned short endGoalYPos;
	unsigned short goalXPos;
	unsigned short goalYPos;

	unsigned short mapSizeX;
	unsigned short mapSizeY;
};

