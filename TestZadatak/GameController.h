#pragma once
#include "Level.h"
#include <string>
#include<conio.h>
#include <json/value.h>
class GameController
{
public:

	GameController(string fileName);
	void initGame(string levelFileName);
	void playGame(bool isNewGame);
	void playerMove();
	void intro();
	void mainMenu();
	void inGameMenu();
	void openInventory();
	void saveGame();
	void loadGame();
	bool askForSave();

	string getGameState();
private:

	bool isInGame = false;
	bool isInMenu = false;
	bool isInGameMenu = false;
	string levelFileName;

	//vector<item> inventory;

	Player player;
	Level level;
};

