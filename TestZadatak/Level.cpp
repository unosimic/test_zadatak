#include "Level.h"
#include <iterator>
#include <Windows.h>

Level::Level()
{
	
}

string Level::getMapFromJson(string mapName) const
{
	Json::Value root;   // will contain the root value after parsing.
	Json::CharReaderBuilder builder;
	ifstream test(mapName, std::ifstream::binary);
	string errs;
	bool ok = Json::parseFromStream(builder, test, &root, &errs);
	if (!ok)
	{
		// report to the user the failure and their locations in the document.
		std::cout << errs << "\n";
		return false;
	}//"../Config/" + level.getMapFromJson(levelFileName) + ".json"
	return "../Config/" + root["map_name"].asString() + ".json";
}

//process json file and load 
Json::Value Level::processJson(Json::Value arr, unsigned short x, unsigned short y, Player &player) {
	//load map
	for (int i = 0; i < arr["map"].size(); i++)
	{
		if (arr["map"][i]["x"] == x && arr["map"][i]["y"] == y)
			return arr["map"][i];
	}
	//load enemies
	for (int i = 0; i < arr["enemy"].size(); i++)
	{
		if (arr["enemy"][i]["x"] == x && arr["enemy"][i]["y"] == y)
		{

			CreateEnemies(arr["enemy"][i]["name"].asString(), arr["enemy"][i]["symbol"].asString()[0], arr["enemy"][i]["attack"].asInt(),
						  arr["enemy"][i]["defense"].asInt(), arr["enemy"][i]["health"].asInt(), arr["enemy"][i]["attack_range"].asInt(),  arr["enemy"][i]["experience_points"].asInt(), arr["enemy"][i]["y"].asInt(), arr["enemy"][i]["x"].asInt());
			return arr["enemy"][i];
		}
	}
	//load map items
	for (int i = 0; i < arr["map_items"].size(); i++)
	{
		if (arr["map_items"][i]["x"] == x && arr["map_items"][i]["y"] == y)
		{
			loadItems(arr["map_items"][i]["name"].asString(), arr["map_items"][i]["type"].asInt(), arr["map_items"][i]["value"].asInt(), arr["map_items"][i]["x"].asInt(), arr["map_items"][i]["y"].asInt(),arr["map_items"][i]["symbol"].asString()[0]);
			return arr["map_items"][i];
		}
	}
	//load player
	if (arr["player"]["x"] == x && arr["player"]["y"] == y)
	{	
		player.setName(arr["player"]["name"].asString());
		player.setPosition(arr["player"]["y"].asInt(), arr["player"]["x"].asInt());
		player.setAttack(arr["player"]["attack"].asInt());
		player.setDefense(arr["player"]["defense"].asInt());
		player.setHealth(arr["player"]["health"].asInt());
		player.setLevel(arr["player"]["level"].asInt());
		player.setExp(arr["player"]["experience"].asInt());
		if (arr["player"]["symbol"].isInt())
		{
			player.setSymbol(arr["player"]["symbol"].asInt());
		}
		else if (arr["player"]["symbol"].isString())
		{
			player.setSymbol(arr["player"]["symbol"].asString()[0]);
		}
		//if (player.
		if (arr["equipped_armor"]["name"].asString() != "")
		{
			//Item *item = new Item(arr["equippedArmor"]["name"].asString(), arr["equippedArmor"]["type"].asInt(), arr["equippedArmor"]["value"].asInt());
			
			Item item(arr["equipped_armor"]["name"].asString(), arr["equipped_armor"]["type"].asInt(), arr["equipped_armor"]["value"].asInt());
			player.setEquippedArmor(item);
		}
		if (arr["equipped_weapon"]["name"].asString() != "")
		{
			Item item2(arr["equipped_weapon"]["name"].asString(), arr["equipped_weapon"]["type"].asInt(), arr["equipped_weapon"]["value"].asInt());
			player.setEquippedWeapon(item2);
			
		}
		return arr["player"];
	}
	//load goals
	if (arr["goal"]["x"] == x && arr["goal"]["y"] == y)
	{	
		goalXPos = x;
		goalYPos = y;
		goalSymbol = arr["goal"]["symbol"].asString()[0];
		player.setIsGoalAchieved(arr["goal"]["state"].asBool());
		if (!(arr["goal"]["state"].asBool()))
		{
			return arr["goal"];
		}
		else
		{
			return 0;
		}
	}
	if (arr["end_goal"]["x"] == x && arr["end_goal"]["y"] == y)
	{
		endGoalSymbol = arr["end_goal"]["symbol"].asString()[0];
		endGoalXPos = x;
		endGoalYPos = y;
		return arr["end_goal"];
	}
	return 0;
}

void Level::loadInventory(Player &player, string name, unsigned short type, unsigned short value)
{
	Item item(name, type, value);
	Item &rItem = item;
	player.addItemToInv(rItem);
}

//create enemies on map
void Level::CreateEnemies(string name, char tile, unsigned short attack, unsigned short defense, unsigned short health,
	unsigned short attackRange, unsigned short expPoints, unsigned short x, unsigned short y)
{
	enemies.push_back(Enemy(name, tile, attack, defense, health, attackRange, expPoints, x, y));
}

//create items on map 
void Level::loadItems(string name, unsigned short type, unsigned short value, unsigned short x, unsigned short y, char symbol)
{
	//Item item(name, type, value, x, y, symbol);
	items.push_back(Item(name,type,value,x,y,symbol));	
}

//load level
bool Level::load(string fileName, Player &player)
{
	
	Json::Value root;   // will contain the root value after parsing.
	Json::CharReaderBuilder builder;
	ifstream test(fileName, std::ifstream::binary);
	string errs;
	bool ok = Json::parseFromStream(builder, test, &root, &errs);
	if (!ok)
	{
		// report to the user the failure and their locations in the document.
		std::cout << errs << "\n";
		return false;
	}
	// load map size
	mapSizeX = root["map_size"]["x"].asInt();
	mapSizeY = root["map_size"]["y"].asInt();

	clearConsole();
	string tempString;

	for (int i = 0; i <= mapSizeX;i++)
	{
		for (int j = 0; j <= mapSizeY; j++)
		{
			Json::Value coordinate = processJson(root, i, j, player);
			if (coordinate == 0) { // empty
				tempString.append(".");				
			}
			else {
				tempString.append(coordinate["symbol"].asString());			
			}
		}
		levelData.push_back(tempString);
		tempString = "";
	}
	for (int i = 0; i < root["player_inventory"].size(); i++)
	{
		//string name = root["playerInventory"][i]["name"].asString();
		//int type = root["playerInventory"][i]["type"].asInt();
		//int value = root["playerInventory"][i]["value"].asInt();
		//loadInventory(player, name, type, value);
		loadInventory(player,
			root["player_inventory"][i]["name"].asString(), 
			root["player_inventory"][i]["type"].asInt(), 
			root["player_inventory"][i]["value"].asInt()
		);
	}
	return true;
}

void Level::save(Player &player, string filename)
{
	Json::Value root;   // will contain the root value after parsing.
	int MapIndex = 0;
	int coordinateIndex = 0;
	int playerIndex = 0;

	for (int i = 0; i < levelData.size(); i++)
	{
		for (int j = 0; j < levelData[i].size(); j++)
		{	//save walls
			if (levelData[i][j] == '#')	
			{
					root["map"][MapIndex]["symbol"] = "#";
					root["map"][MapIndex]["type"] = "WALL";
					root["map"][MapIndex]["y"] = j;
					root["map"][MapIndex]["x"] = i;
					MapIndex++;
				}			
		}
		// save items on map
		for (int i = 0; i < items.size(); i++)
		{ 
			root["map_items"][i]["x"] = items[i].getXPos();
			root["map_items"][i]["y"] = items[i].getYPos();
			root["map_items"][i]["type"] = items[i].getType();
			root["map_items"][i]["value"] = items[i].getValue();
			root["map_items"][i]["name"] = items[i].getName();
			root["map_items"][i]["symbol"] = string(1,items[i].getSymbol());
		}
		//save map size
		root["map_size"]["x"] = mapSizeX;
		root["map_size"]["y"] = mapSizeY;
	}
	//save player
	unsigned short playerX;
	unsigned short playerY;
	player.getPosition(playerX, playerY);
	root["player"]["name"] = player.getName();
	root["player"]["symbol"] = string(1,player.getSymbol());
	root["player"]["defense"] = player.getDefense();
	root["player"]["attack"] = player.getAttack();
	root["player"]["health"] = player.getHealth();
	root["player"]["level"] = player.getLevel();
	root["player"]["experience"] = player.getExp();
	root["player"]["y"] = playerX;
	root["player"]["x"] = playerY;
	//save enemies
	for (int i = 0; i < enemies.size(); i++)
	{
		unsigned short enemyX;
		unsigned short enemyY;
		enemies[i].getPosition(enemyX, enemyY);

		root["enemy"][i]["name"] = enemies[i].getName();
		root["enemy"][i]["x"] = enemyY;
		root["enemy"][i]["y"] = enemyX;
		root["enemy"][i]["experience_points"] = enemies[i].getExpPoints();
		root["enemy"][i]["health"] = enemies[i].getHealth();
		root["enemy"][i]["attack"] = enemies[i].getAttack();
		root["enemy"][i]["defense"] = enemies[i].getDefense();
		root["enemy"][i]["attack_range"] = enemies[i].getAttackRange();
		root["enemy"][i]["symbol"] = string(1,(enemies[i].getSymbol()));
	}
	// save player inventory
	//auto tempInv = player.getInventory();
	for (int i = 0; i < player.getInventory().size(); i++)
	{
		root["player_inventory"][i]["name"] = player.getInventory()[i].getName();
		root["player_inventory"][i]["type"] = player.getInventory()[i].getType();
		root["player_inventory"][i]["value"] = player.getInventory()[i].getValue();
	}

	// save equipped items
	if (player.getIsEquippedWeapon())
	{
		root["equipped_weapon"]["name"] = player.getEquippedWeapon().getName();
		root["equipped_weapon"]["type"] = player.getEquippedWeapon().getType();
		root["equipped_weapon"]["value"] = player.getEquippedWeapon().getValue();
	}
	if (player.getIsEquippedArmor())
	{
		root["equipped_armor"]["name"] = player.getEquippedArmor().getName();
		root["equipped_armor"]["type"] = player.getEquippedArmor().getType();
		root["equipped_armor"]["value"] = player.getEquippedArmor().getValue();
	}

	//save goals
	root["goal"]["x"] = goalXPos;
	root["goal"]["y"] = goalYPos;
	root["goal"]["symbol"] = string(1,goalSymbol);
	root["goal"]["state"] = player.GetIsGoalAchieved();

	root["end_goal"]["x"] = endGoalXPos;
	root["end_goal"]["y"] = endGoalYPos;
	root["end_goal"]["symbol"] = string(1, endGoalSymbol);


	Json::StreamWriterBuilder wBuilder;
	ofstream outFile(filename);
	std::unique_ptr<Json::StreamWriter> writer(wBuilder.newStreamWriter());
	writer->write(root, &outFile);
}

void Level::clearLevel(Player &player)
{
	levelData.clear();
	enemies.clear();
	items.clear();
	player.clearInventory();
	player.setIsGoalAchieved(false);
	player.deleteEquippedItems();
	//Item empty("", 0, 0);
	//player.setEquippedArmor(empty);
	//Item emptyW("", 1, 0);
	//player.setEquippedWeapon(empty);
}

//print level
void Level::print(Player &player)
{
	printf("%s Stats : \n%s\n\n", player.getName().c_str(), player.getAsString().c_str());
	for (int i = 0; i < levelData.size(); i++)
	{		
		printf("%s\n", levelData[i].c_str());
	}
	printf("\n");
}

void Level::drawIntro(Player &player)
{
	cout << "Welcome " << player.getName() << endl;
	cout << "Your goal is to find item :" << goalSymbol << " and escape" << endl;
	cout << "To escape you need to go to '" << endGoalSymbol << "'" << endl;
	cout << "Enemies are represented with letters" << endl;
	pauseConsole(" ");
}
//draw menu and return user input
int Level::drawMainMenu()
{
	clearConsole();
	int choice;
	cout << "\x1b[97m       |\\                  /)\033[0m" << endl;
	cout << "\x1b[37m     /\\_\\\\__            (_//\033[0m" << endl;
	cout << "    |   `>\\-`   \x1B[33m_._\033[0m      //`)" << endl;
	cout << "     \\ /` \\\\\x1B[33m_.-`:::`-._ \033[0m//" << endl;
	cout << "      `  \x1B[33m/`     :::     `\\\033[0m" << endl;
	cout << "         >   \x1B[33mMain MENU\033[0m   <" << endl;
	cout << "         |_______________|" << endl;
	cout << "      1 :[Start new game ]" << endl;
	cout << "      2 :[   Load game   ]" << endl;
	cout << "      3 :[      Quit     ]" << endl;
	cout << "\x1B[33m         \\      :::      /\033[0m" << endl;
	cout << "\x1B[33m          \\     :::     /\033[0m" << endl;
	cout << "\x1B[33m           `-.  :::  .-'\033[0m" << endl;
	cout << "             //\x1B[33m`:::`\033[0m\\\\" << endl;
	cout << "            // \x1B[33m  '   \033[0m\\\\" << endl;
	cout << "           |/         \\\\" << endl;
	cout << endl << "Choice : ";
	
	cin >> choice;

	return choice;
}

int Level::drawInGameMenu()
{
	clearConsole();
	int choice;
	cout << "\x1B[33mGame Menu\033[0m\t\t" << endl;
	cout << "1 : Resume Game" << endl;
	cout << "2 : Save Game" << endl;
	cout << "3 : Load Game" << endl;
	cout << "4 : Inventory" << endl;
	cout << "5 : Back To Main Menu" << endl;
	cout << "6 : Quit" << endl;
	cout << endl << "Choice : ";
	cin >> choice;

	return choice;
}

int Level::drawInventory(Player &player)
{
	clearConsole();
	cout << "\x1B[33mInventory\033[0m" << endl;
	cout << player.getInventoryAsString() << endl;
	if (player.getIsEquippedArmor() || player.getIsEquippedWeapon())
	{
		cout << "Equipped : " << player.getEquippedArmor().getName() << "\t" << player.getEquippedWeapon().getName() << "\n";
	}
	if (player.getInventoryAsString().size() > 0)
	{
		cout << "\nDo you want to equip an item?[y][n]: " << endl;

		char choice;
		cin >> choice;
		switch (choice)
		{
		case 'y':
			cout << "Choose item to equip: " << endl;
			int itemIndex;
			cin >> itemIndex;
			if (itemIndex - 1 > player.getInventory().size())
			{
				return -1;
				
			}
			else
				return itemIndex;
		case 'n':
			return 0;

		default:
			return -1;

		}
	}
	else
	{
		cout << "Inventory is empty\n";
		pauseConsole("Press enter to return to menu...");
		return 0;
	}
}

bool Level::movePlayer(char input, Player &player)
{
	unsigned short playerX;
	unsigned short playerY;
	player.getPosition(playerX, playerY);
	switch (input)
	{
	case 'w':
	case 'W':	//up
		processPlayerMove(player, playerX, playerY - 1);
		return true;
	case 's':
	case 'S':	//Down
		processPlayerMove(player, playerX, playerY + 1);
		return true;
	case 'a':
	case 'A':	//Left
		processPlayerMove(player, playerX - 1, playerY);
		return true;
	case 'd':
	case 'D':	//Right
		processPlayerMove(player, playerX + 1, playerY);
		return true;

	default:
		cout << "Wrong command!\t";
		return false;
	}
}

void Level::MoveEnemies(Player & player)
{
	char EnemyMove;
	unsigned short playerX;
	unsigned short playerY;

	unsigned short enemyPosX;
	unsigned short enemyPosY;

	player.getPosition(playerX, playerY);
	for (int i = 0; i < enemies.size(); i++)
	{
		enemies[i].getPosition(enemyPosX, enemyPosY);
		EnemyMove = enemies[i].getMoveCommand(playerX, playerY);

		switch (EnemyMove)
		{
		case 'w':
			processEnemyMove(player, i, enemyPosX, enemyPosY - 1);
			break;
		case 's':
			processEnemyMove(player, i, enemyPosX, enemyPosY +1);
			break;
		case 'a':
			processEnemyMove(player, i, enemyPosX - 1, enemyPosY);
			break;
		case 'd':
			processEnemyMove(player, i, enemyPosX + 1, enemyPosY);
			break;
		}
	}
}

void Level::setTile(unsigned short x, unsigned short y, char tile)
{
	this->levelData[y][x] = tile;
}

char Level::getTile(unsigned short x, unsigned short y) const
{
	return this->levelData[y][x];
}

vector<string> Level::getLevelData() const
{
	return levelData;
}

void Level::spawnItem(unsigned short x, unsigned short y)
{

	static default_random_engine randomEngine(time(NULL));
	uniform_int_distribution<int>randValue(0, 2);
	int chance = randValue(randomEngine);

	Item item(chance, y, x);
	items.push_back(item);
	setTile(x, y, '?');
	cout << "Spawned item: " << item.getName() << endl;
	pauseConsole();
}

void Level::processPlayerMove(Player & player, unsigned short targetX, unsigned short targetY)
{
	unsigned short playerX;
	unsigned short playerY;
	player.getPosition(playerX, playerY);
	char moveTile = getTile(targetX, targetY);

	if (moveTile == '.')
	{
		//clearConsole();
		player.setPosition(targetX, targetY);
		setTile(playerX, playerY, '.');
		setTile(targetX, targetY, player.getSymbol());		
	}
	else if (moveTile == '?')
	{
		clearConsole();
		for (int i = 0; i < items.size(); i++)
		{
			if (items[i].getXPos() == targetY && items[i].getYPos() == targetX)
			{
				//add weapon to player
				player.addItemToInv(items[i]);

				cout << "Picked item: " << items[i].getName() << endl;

				//delete weapon from map 
				items[i] = items.back();
				items.pop_back();
				pauseConsole();
				break;
			}
		}
		player.setPosition(targetX, targetY);
		setTile(playerX, playerY, '.');
		setTile(targetX, targetY, player.getSymbol());
		//clearConsole();
	}
	else if (moveTile == '#')
	{
		//clearConsole();
	}
	else if (moveTile == goalSymbol)
	{
		clearConsole();
		cout << "You picked up your goal. Now fight your way back! \n";
		pauseConsole();
		player.setIsGoalAchieved(true);
		player.setPosition(targetX, targetY);
		setTile(playerX, playerY, '.');
		setTile(targetX, targetY, player.getSymbol());
	}
	else if (moveTile == endGoalSymbol)
	{
		if (player.GetIsGoalAchieved())
		{
			cout << "YOU WON!\n";
			pauseConsole();
			exit(0);
		}
		else
		{
			cout << "You need to pick up goal to win\n";			
			pauseConsole();
			clearConsole();			
		}
	}
	else
	{
		clearConsole();
		battleExecution(player, targetX, targetY, true);
	}
	clearConsole();
}

void Level::processEnemyMove(Player & player, unsigned short index, unsigned short targetX, unsigned short targetY )
{
	unsigned short playerX;
	unsigned short playerY;
	unsigned short enemyX;
	unsigned short enemyY;
	const char playerSymbol = player.getSymbol();
	enemies[index].getPosition(enemyX, enemyY);
	player.getPosition(playerX, playerY);
	char moveTile = getTile(targetX, targetY);
	
	if (moveTile == '.')
	{
		enemies[index].setPosition(targetX, targetY);
		setTile(enemyX, enemyY, '.');
		setTile(targetX, targetY, enemies[index].getSymbol());
	}
	else if (moveTile == player.getSymbol())
	{
		clearConsole();
		battleExecution(player, enemyX, enemyY, false);
	}	
}

void Level::battleExecution(Player & player, unsigned short targetX, unsigned short targetY, bool whoAttacked)
{
	
	unsigned short enemyX;
	unsigned short enemyY;

	unsigned short playerX;
	unsigned short playerY;

	int attackValue;
	bool attackResult;

	player.getPosition(playerX, playerY);
	print(player);
	for (int i = 0; i < enemies.size(); i++)
	{
		this->enemies[i].getPosition(enemyX, enemyY);

		if (targetX == enemyX && targetY == enemyY)
		{
			if (whoAttacked) // player attacked
			{

				attackValue = player.attackMove();
				attackResult = this->enemies[i].takeDamage(attackValue);
				cout << "You attacked enemy: " << enemies[i].getName() << " with " << attackValue << " attack" << endl;
				cout << enemies[i].getName() << " stats: HP: " << enemies[i].getHealth() << " Attack :" << enemies[i].getAttack() << " Defense: " << enemies[i].getDefense() << endl;

				pauseConsole();
				//clearConsole();

				if (attackResult) // if enemy died
				{
					cout << enemies[i].getName() << " died!\n";
					cout << "You gained " << enemies[i].getExpPoints() << " experience points!" << endl;
					if (rand() % 2 == 0)
					{					
						setTile(targetX, targetY, '.');
						pauseConsole();
					}
					else
					{
						spawnItem(targetX, targetY);
					}
					player.addExperience(enemies[i].getExpPoints());
					//remove enemy from list and lower the index so it doesn't skip enemy
					enemies[i] = enemies.back();
					enemies.pop_back();
					i--;
					
					clearConsole();
					break;
					//pauseConsole();
				}
				else //if enemy survived
				{
					attackValue = enemies[i].attackMove();
					cout << "Enemy " << enemies[i].getName() << " attacked you with " << attackValue << " attack!" << endl;
					attackResult = player.takeDamage(attackValue);
					cout << enemies[i].getName() << " stats: HP: " << enemies[i].getHealth() << " Attack :" << enemies[i].getAttack() << " Defense: " << enemies[i].getDefense() << endl;
					pauseConsole();
					if (attackResult)
					{
						setTile(playerX, playerY, 'X');
						//print(player);
						clearConsole();
						cout << "You died!\nGame over...\n";
						cout << enemies[i].getName() << " killed you";
						pauseConsole();
						exit(0);
					}
					break;
				}
			}
			else // enemy attack
			{
				attackValue = enemies[i].attackMove();
				cout << "Enemy " << enemies[i].getName() << " attacked you with " << attackValue << " attack!" << endl;
				attackResult = player.takeDamage(attackValue);
				cout << enemies[i].getName() << " stats: HP: " << enemies[i].getHealth() << " Attack :" << enemies[i].getAttack() << " Defense: " << enemies[i].getDefense() << endl;
				pauseConsole("Press enter to fight back");
				//clearConsole();
				if (attackResult) // if player died
				{
					setTile(playerX, playerY, 'X');
					//print(player);
					clearConsole();
					cout << "You died!\nGame over...\n";
					cout << enemies[i].getName() << " killed you\n";
					pauseConsole();
					exit(0);
					//return;
				}

				else //if player survived
				{
					attackValue = player.attackMove();
					attackResult = this->enemies[i].takeDamage(attackValue);
					cout << "You attacked enemy: " << enemies[i].getName() << " with " << attackValue << " attack" << endl;
					cout << enemies[i].getName() << " stats: HP: " << enemies[i].getHealth() << "Attack :" << enemies[i].getAttack() << "Defense: " << enemies[i].getDefense() << endl;
					pauseConsole();
					//system("PAUSE");
					if (attackResult) // if enemy died
					{
						cout << enemies[i].getName() << " died!\n";
						cout << "You gained " << enemies[i].getExpPoints() << " experience points!" << endl;
						if (rand() % 2 == 0)
						{
							setTile(targetX, targetY, '.');
							pauseConsole();
						}
						else
						{
							spawnItem(targetX, targetY);
						}
						//print(player);
						player.addExperience(enemies[i].getExpPoints());
						//remove enemy from list and lower the index so it doesn't skip enemy
						enemies[i] = enemies.back();
						enemies.pop_back();
						i--;
						
						clearConsole();
						break;

					}
					clearConsole();
					break;
				}
		}
		}
	}
	
}

void Level::clearConsole() { //Clears console
#ifdef _WIN32 //Windows
	system("CLS");
#else //Others
	system("clear");
#endif
}

void Level::pauseConsole(string textToShow)
{
	do
	{
		cout << textToShow << "\n";
	} while (cin.get() != '\n');
}

