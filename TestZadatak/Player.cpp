#include "Player.h"


Player::Player() 
{
	this->x = 0;
	this->y = 0;
	pItemWeap = new Item;
	pItemWeap = 0;//&equippedWeapon;

	pItemArm = new Item;
	pItemArm = 0;//&equippedArmor;
	//this->isEquippedArmor = false;
	//this->isEquippedWeapon = false;
}

// initialize player
void Player::init(string name)//, unsigned short health, unsigned short attack, unsigned short defense)
{
	this->name = name;
	//this->health = health;
	//this->attack = attack;
	//this->defense = defense;
	//this->level = 1;
	//this->exp = 0;
	this->expNext =
		(50 / 3)*(pow(level, 3) -
			6 * pow(level, 3) +
			(17 * level) - 11);
	
}

void Player::levelUp()
{
	while (exp >= expNext)
	{
		exp -= expNext;
		level++;
		this->exp = 0;
		this->expNext =
			(50 / 3)*(pow(level, 3) -
				6 * pow(level, 3) +
				(17 * level) - 11);
	}
}
void Player::addExperience(USHORT experience)
{
	this->exp += experience;
		if (exp >= expNext)
		{
			this->level += 1;
			this->exp -= expNext;
			this->expNext =
				(50 / 3)*(pow(this->level, 3) -
					6 * pow(this->level, 2) +
					(17 * this->level) - 12);
			this->attack += 10 * (this->level -1);
			this->health += 15 * (this->level - 1);
			this->defense += 10 * (this->level - 1);

		}
}

void Player::setLevel(USHORT level)
{
	this->level = level;
}

void Player::setExp(int exp)
{
	this->exp = exp;
}

int Player::attackMove()
{
	static default_random_engine randomEngine(time(NULL));
	uniform_int_distribution<int>attackStrength(attack/2, attack);

	return attackStrength(randomEngine);
}

void Player::setPosition(unsigned short x, unsigned short y)
{
	this->x = x;
	this->y = y;
}

void Player::getPosition(unsigned short &x, unsigned short &y) const
{
	x = this->x;
	y = this->y;
}

bool Player::takeDamage(int attackValue)
{
	int tempAttackValue = attackValue;
	attackValue -= defense;
	if (attackValue > 0)
	{
		defense = 0;
		health -= attackValue;
		equippedArmor.setValue(0);
		//check if he died
		if (health <= 0)
		{
			return true;
		}
	}
	else
	{
		defense -= tempAttackValue;
		int value = equippedArmor.getValue() - attackValue;
		equippedArmor.setValue(value);
		//if (defense < 0)
		//{
		//	defense = 0;
		//}
	}
	return false;
}

//return player stats for print
string Player::getAsString() const
{
	return "HP : " + to_string(health)
		+ " Attack : " + to_string(attack)
		+ " Defense : " + to_string(defense)
		+ " Level : " + to_string(level)
		+ " \nXP : " + to_string(exp)
		+ " XP^ : " + to_string(expNext)
		+ " Goal : " + to_string(isGoalAchieved);
		
}

void Player::setName(string name)
{
	this->name = name;
}

void Player::addItemToInv(Item &item)
{
	inventory.push_back(item);
}

vector<Item>& Player::getInventory()
{
	vector<Item> &rInventory = inventory;
	return rInventory;

	//return inventory;
}

void Player::euqipItem(unsigned short index)
{
	Item currentItem = inventory[index];

	inventory[index] = inventory.back();
	inventory.pop_back();

	if (currentItem.getType() == 0)
	{		
		if (isEquippedWeapon)
		{
			
			inventory.push_back(getEquippedWeapon());
			this->attack -= equippedWeapon.getValue();
			cout << "Item: " << equippedWeapon.getName() << "  returned to inventory.\n";
		}
		pItemWeap = &equippedWeapon;
		equippedWeapon = currentItem;
		isEquippedWeapon = true;
		this->attack += currentItem.getValue();
		cout << "Attack is now " << attack << endl;
	}

	else if (currentItem.getType() == 1)
	{
		if (isEquippedArmor)
		{
			this->defense -= equippedArmor.getValue();
			cout << "Item: " << equippedArmor.getName() << "  returned to inventory.\n";
			inventory.push_back(equippedArmor);
		}
		pItemArm = &equippedArmor;
		equippedArmor = currentItem;
		isEquippedArmor = true;
		this->defense += currentItem.getValue();
		cout << "Defense is now " << defense << endl;
	}
	else if (currentItem.getType() == 2)
	{
		this->health += currentItem.getValue();
		if (health > 100)
			health = 100;
		cout << "Health is now " << health << endl;
	}
}

const string Player::getInventoryAsString() const
{
	string inventoryStr;
	for (int i = 0; i < inventory.size(); i++)
	{
		inventoryStr.append(to_string(i+1)+" : " + "~~> " + inventory[i].getName() +
			" value: " + to_string(inventory[i].getValue()) + "\n");
	}
	return inventoryStr;
}

void Player::setAttack(unsigned short attack)
{
	this->attack = attack;
}

void Player::setDefense(unsigned short defense)
{
	this->defense = defense;
}

void Player::setHealth(unsigned short health)
{
	this->health = health;
}

void Player::setSymbol(char symbol)
{
	this->symbol = symbol; ;
}

Item& Player::getEquippedWeapon() 
{		
	return equippedWeapon;
}

Item& Player::getEquippedArmor()
{
	return equippedArmor;
}

bool Player::GetIsGoalAchieved() const
{
	return isGoalAchieved;
}

void Player::setEquippedWeapon(Item &item)
{ // TODO: SREDI OVO
	isEquippedWeapon = true;
	Item *pItemtest = new Item;
	*pItemtest = item;
	pItemWeap = pItemtest;
	equippedWeapon = *pItemWeap;
}
void Player::deleteEquippedArmor(Item& item)
{
	delete &item;
}
void Player::deleteEquippedItems()
{
	//if (pItemWeap != NULL)
	//	delete pItemWeap;

	//if (pItemArm != NULL)
	//	delete pItemArm;
	//if (&rEquippedArmor != NULL)
	//	delete &rEquippedArmor;

	isEquippedArmor = false;
	isEquippedWeapon = false;
}
void Player::setEquippedArmor(Item &item)
{
	isEquippedArmor = true;
	equippedArmor = item;
}

void Player::setIsGoalAchieved(bool isGoalAchieved)
{
	this->isGoalAchieved = isGoalAchieved;
}

void Player::clearInventory()
{
	inventory.clear();
	//deleteEquippedArmor(equippedArmor);
	//deleteEquippedWeapon(equippedWeapon);
	//isEquippedArmor = false;
	//isEquippedWeapon = false;
}
