#pragma once
#include <random>
#include <ctime>
#include <string>
using namespace std;
class Item 
{
public:
	Item(unsigned short type, unsigned short x, unsigned short y);
	Item(string name, unsigned short type, unsigned short value, unsigned short x, unsigned short y, char symbol);
	Item(string name, unsigned short type, unsigned short value);
	Item();
	~Item();

	inline const string& getName() const { return name; };
	inline const int& getType() const {return type; };
	inline const int& getValue() const { return value; };
	inline const unsigned short& getXPos() const { return x; };
	inline const unsigned short& getYPos() const { return y; };
	inline const char& getSymbol() const { return symbol; };

	void setValue(unsigned short value);
private:
	unsigned short type; //0 weapon, 1 armor, 2 potion
	string name;
	 char symbol;
	unsigned short value;
	//position
	unsigned short x;
	unsigned short y;
};

