#pragma once
#include<string>
#include"Item.h"
#include<iostream>
#include<vector>
using namespace std;
typedef unsigned short int USHORT;
class Player
{
	
public:
	Player();

	//Functions
	void levelUp();
	void init(string name);//, USHORT health, USHORT attack, USHORT defense);
	int attackMove();
	bool takeDamage(int attackValue);
	void addItemToInv(Item &item);
	vector<Item>& getInventory();
	void euqipItem(USHORT index);
	void clearInventory(); //TODO: delete items
	void deleteEquippedArmor(Item& item);
	void deleteEquippedItems();
	//getters
	void getPosition(USHORT &x, USHORT &y) const;
	inline const string& getName() const { return this->name; };
	inline const int& getHealth() const { return this->health; };
	inline const USHORT& getAttack() const { return this->attack; };
	inline const unsigned int& getExp() const { return this->exp; };
	inline const USHORT& getDefense() const { return this->defense; };
	inline const USHORT& getLevel() const { return this->level; };
	inline const char& getSymbol() const { return this->symbol; };
	
	inline const bool getIsEquippedWeapon() const { return this->isEquippedWeapon; };
	inline const bool getIsEquippedArmor() const { return this->isEquippedArmor; };

	const string getInventoryAsString() const;
	Item& getEquippedWeapon();
	Item& getEquippedArmor();
	bool GetIsGoalAchieved() const;
	string getAsString() const;


	//setters
	void setName(string name);
	void setPosition(USHORT x, USHORT y);
	void setAttack(USHORT attack);
	void setDefense(USHORT defense);
	void setHealth(USHORT health);
	void setSymbol(char symbol);
	void setEquippedWeapon(Item &item);
	void setEquippedArmor(Item &item);
	void setIsGoalAchieved(bool isGoalAchieved);
	void addExperience(USHORT experience);
	void setLevel(USHORT level);
	void setExp(int exp);

private:
	Item *pItemWeap; 
	Item *pItemArm;
	vector <Item> inventory;
	//vector <Item> &inventory;
	Item equippedArmor;
	Item equippedWeapon;
	Item &rEquippedArmor = equippedArmor;
	Item &rEquippedWeapon = equippedWeapon;

	string name;
	USHORT level;
	unsigned int expNext;
	int exp;
	int health;
	USHORT attack;
	int defense;
	char symbol;
	bool isEquippedWeapon = false;
	bool isEquippedArmor = false;
	bool isGoalAchieved;

	//position
	USHORT x;
	USHORT y;
};

