#include "Item.h"
#include<iostream>

Item::Item(unsigned short type, unsigned short x, unsigned short y)
{
	static default_random_engine randomEngine(time(NULL));
	uniform_int_distribution<unsigned short>nameRand(0, 2);
	uniform_int_distribution<unsigned short>valueRand(1, 10);
	uniform_int_distribution<unsigned short>valueRand2(21, 40);
	uniform_int_distribution<unsigned short>valueRand3(41, 80);

	this->type = type;
	unsigned short chance;
	switch (type)
	{
	case 0:		//Weapon

		chance = nameRand(randomEngine);
		switch (chance)
		{
		case 0:
			this->name = "Light Sword cons";
			this->value = valueRand(randomEngine);
			break;
		case 1:
			this->name = "Heavy Sword cons";
			this->value = valueRand2(randomEngine);
			break;
		case 2:
			this->name = "Legendary Sword cons";
			this->value = valueRand3(randomEngine);
			break;
		}
		break;


	case 1:		//Armor
			chance = nameRand(randomEngine);
			switch (chance)
			{
			case 0:
				this->name = "Light Armor cons";
				this->value = valueRand(randomEngine);
				break;
			case 1:
				this->name = "Heavy Armor cons";
				this->value = valueRand2(randomEngine);
				break;
			case 2:
				this->name = "Legendary Armor cons";
				this->value = valueRand3(randomEngine);
				break;
			}
			break;

	case 2: // Potion
		chance = nameRand(randomEngine);
		switch (chance)
		{
		case 0:
			this->name = "Small Health Potion cons";
			this->value = valueRand(randomEngine) + 10;
			break;
		case 1:
			this->name = "Large Health potion cons";
			this->value = valueRand2(randomEngine) + 10;
			break;
		case 2:
			this->name = "Legendary Health potion cons";
			this->value = 100;
			break;
		}
		break;
	}
	this->x = x;
	this->y = y;

}
Item::Item(string name, unsigned short type, unsigned short value, unsigned short x, unsigned short y, char symbol)
{
	this->name = name;
	this->type = type;
	this->value = value;
	this->x = x;
	this->y = y;
	this->symbol = symbol;
}
Item::Item(string name, unsigned short type, unsigned short value)
{
	this->name = name;
	this->type = type;
	this->value = value;
	this->x = 0;
	this->y = 0;
}

void Item::setValue(unsigned short value)
{
	this->value = value;
}

Item::Item()
{}

Item::~Item()
{
}

