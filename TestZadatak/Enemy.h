#pragma once
#include<string>
#include<vector>
#include <random>
#include <ctime>

using namespace std;
class Enemy
{
public:
	Enemy(string name, char tile, unsigned short attack, unsigned short defense, unsigned short health,
		unsigned short attackRange, unsigned short expPoints, unsigned short x, unsigned short y);
	~Enemy();

	//functions
	unsigned short attackMove();
	bool takeDamage(int attackValue);
	char getMoveCommand(unsigned short playerX, unsigned short playerY);

	//setters
	void setPosition(unsigned short x, unsigned short y);

	//getters
	void getPosition(unsigned short &x, unsigned short &y) const;
	inline const int& getHealth() const { return this->health; };
	inline const unsigned short& getAttack() const { return this->attack; };
	inline const int& getDefense() const { return this->defense; };
	inline const char& getSymbol() const { return this->symbol; };
	inline const unsigned short& getAttackRange() const { return this->attackRange; };
	inline const string& getName() const { return this->name; };
	inline const unsigned short& getExpPoints() const { return this->expPoints; };
	vector <string> getAsString() const;

private:

	string name;
	char symbol;
	unsigned short expPoints;

	unsigned short attack;
	int defense;
	int health;
	unsigned short attackRange;

	unsigned short x;
	unsigned short y;
};

