CONTROL ec4350cb5cf5dd8cabe35e7774083676a981da80
allow-disable-examples.patch ef553461db4989c2e7abfd1ae461476ec6b1cd50
cmake 3.17.2
features core
portfile.cmake b561450b9e0edc991f4bd91653e6165c7125a61e
post_build_checks 2
powershell 2e410fc5e429fc6ad478976eff3efcffe9d53a0620c7c31c2b8bfe1f0976cade74632fc202bf58959cec111f03b824ba42ad4d74c8a66d67ec21bbfe9b9c626d
triplet e8dc96fd479b53bb0e6748625827c8268017e35a-a1c0eabb0c5177b6a8fb97a58ae398880c47b352-831e4ff9921b3649275af1c3d47feccdb1937ec8
vcpkg_configure_cmake d9e525769b8519c26e4c0446242434961527d33b
vcpkg_copy_pdbs 4bbc15802daf8f9f8a92794c10b0a89cfca1e742
vcpkg_fixup_cmake_targets ef088786b9a6815971b8b6bd3a511183e77a8b93
vcpkg_from_git 4834edbcec7eed8d12c6b1ad3c25af63d5fa6586
vcpkg_from_github 4624dffdf2b8d85ffdfde0850369fa96abdf0bb8
vcpkg_install_cmake 75d1079d6f563d87d08c8c5f63b359377aa19a6e
